'use strict';

var gulp = require('gulp'),
	watch = require('gulp-watch'),
	prefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	rigger = require('gulp-rigger'),
	cssmin = require('gulp-minify-css'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	rimraf = require('rimraf');

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/images/',
		fonts: 'build/fonts/',
		tmpl: 'build/tmpl'
	},
	src: {
		html: 'src/*.html',
		js: 'src/js/app.js',
		style: 'src/styles/app.scss',
		img: 'src/images/**/*.*',
		fonts: 'src/fonts/**/*.*',
		tmpl: 'src/tmpl/*.*'
	},
	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		style: 'src/styles/**/*.scss',
		img: 'src/images/**/*.*',
		fonts: 'src/fonts/**/*.*',
		tmpl: 'src/tmpl/*.*'
	},
	clean: './build'
};

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
	gulp.src(path.src.html) 
		.pipe(rigger())
		.pipe(gulp.dest(path.build.html));
});

gulp.task('js:build', function () {
	gulp.src(path.src.js) 
		.pipe(rigger()) 
		.pipe(sourcemaps.init()) 
		.pipe(uglify()) 
		.pipe(sourcemaps.write()) 
		.pipe(gulp.dest(path.build.js));
});

gulp.task('styles:build', function () {
	gulp.src(path.src.style) 
		.pipe(sourcemaps.init())
		.pipe(sass({
			includePaths: ['src/styles/'],
			outputStyle: 'compressed',
			sourceMap: true,
			errLogToConsole: true
		}))
		.pipe(prefixer())
		.pipe(cssmin())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css));
});

gulp.task('images:build', function () {
	gulp.src(path.src.img) 
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()],
			interlaced: true
		}))
		.pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function() {
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('tmpl:build', function () {
	gulp.src(path.src.tmpl) 
		.pipe(rigger())
		.pipe(gulp.dest(path.build.tmpl));
});


gulp.task('build', [
	'html:build',
	'js:build',
	'styles:build',
	'fonts:build',
	'images:build',
	'tmpl:build'
]);


gulp.task('watch', function(){
	gulp.watch([path.watch.html], function(event, cb) {
		gulp.start('html:build');
	});
	gulp.watch([path.watch.style], function(event, cb) {
		gulp.start('styles:build');
	});
	gulp.watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	gulp.watch([path.watch.img], function(event, cb) {
		gulp.start('images:build');
	});
	gulp.watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
	gulp.watch([path.watch.tmpl], function(event, cb) {
		gulp.start('tmpl:build');
	});
});


gulp.task('default', ['build', 'watch']);