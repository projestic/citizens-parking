/*
 Third party
 */

//= ../vendor/lodash/dist/lodash.min.js
//= ../vendor/angular/angular.min.js
//= ../vendor/angular-simple-logger/dist/angular-simple-logger.min.js
//= ../vendor/angular-animate/angular-animate.min.js
//= ../vendor/angular-google-maps/dist/angular-google-maps.min.js
//= ../vendor/angular-bootstrap/ui-bootstrap-tpls.min.js
//= ../vendor/angular-google-places-autocomplete/src/autocomplete.js
//= ../vendor/angular-ui-select/dist/select.min.js

var app = angular.module('app', [
	'ngAnimate',
	'google.places',
	'uiGmapgoogle-maps',
	'ui.bootstrap',
	'ui.select'
]);

//= controllers/apply-ctrl.js
//= controllers/main-ctrl.js
//= controllers/search-ctrl.js
//= controllers/search-result-ctrl.js

//= directives/content-full-height-directive.js