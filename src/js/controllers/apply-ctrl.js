app.controller('ApplyCtrl', ['$rootScope', '$scope', '$uibModal', function($rootScope, $scope, $uibModal){  
    
    $scope.currentParking = {
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: 'images/search-result-pl-180x150.jpg',
        latitude: 33.8676931,
        longitude: -84.832544, 
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    };

    $scope.applyForm = {
        acceptRules: false,
        accountName: null,
        address1: null,
        address2: null,
        city: null,
        companyName: null,
        date: null,
        email: null,
        phone: null,
        state: null,
        zip: null
    };

    //Select states
    $scope.states = [
        {
            id: 1, 
            name: 'Option 1'
        },{
            id: 2, 
            name: 'Option 2'
        },{
            id: 3, 
            name: 'Option 3'
        }
    ];

    $scope.selectedState = null;

    //Datepicker
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1,
        showWeeks: false
    };

    $scope.datePopup = {
        opened: false
    };

    $scope.openDatePopup = function() {
        $scope.datePopup.opened = true;
    };

    //Submit apply form
    $scope.showSuccesModal = false;

    $scope.submitApplyForm = function(){
        if($scope.applyForm.$valid){
            $scope.showSuccesModal = true;
        }
    }

}]);
