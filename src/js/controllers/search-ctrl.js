app.controller('SearchCtrl', ['$rootScope', '$scope', function($rootScope, $scope){ 

    $scope.place = null;

    $scope.autocompleteOptions = {
        componentRestrictions: { 
            country: 'US' 
        },
        types: ['geocode']
    }
}]);
