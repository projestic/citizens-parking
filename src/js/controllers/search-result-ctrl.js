app.controller('SearchResultCtrl', ['$rootScope', '$scope', function($rootScope, $scope){ 

    $scope.searchResult = [{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: 'images/search-result-pl-180x150.jpg',
        latitude: 33.8676931,
        longitude: -84.832544, 
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: 'images/search-result-pl-180x150.jpg',
        latitude: 33.7385709,
        longitude: -84.2306434,
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: null,
        latitude: 33.4676931,
        longitude: -84.5906434, 
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: 'images/search-result-pl-180x150.jpg',
        latitude: 33.3676931,
        longitude: -84.2906434, 
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: null,
        latitude: 34.069982,
        longitude: -84.392772,
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: 'images/search-result-pl-180x150.jpg',
        latitude: 33.7806931,
        longitude: -84.6906434,
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: null,
        latitude: 33.5676931,
        longitude: -84.4906434,
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    },{
        addressRow1: '3200 W. Peachtree',
        addressRow2: 'Rd Atlanta, GA 30326',
        capacity: '250',
        covered: false,
        date: 'M-F, 24hrs',
        distance: '.11 mi',
        image: 'images/search-result-pl-180x150.jpg',
        latitude: 33.6676931,
        longitude: -84.5506434,
        maxHeight: '7’ 5”',
        mountlyRate: '50$',
        operated: 'Lanier Parking',
        overnight: true,
        showAdditionalInfo: false,
        title: 'Ga Pacific Center'
    }];

    $scope.toggleAdditionalInfo = function(item){
        item.showAdditionalInfo = !item.showAdditionalInfo;
    };

    $scope.searchLocation = {
        latitude: 33.7676931,
        longitude: -84.4906434 
    };

    $scope.map = {
        center: {
            latitude: parseFloat($scope.searchLocation.latitude),
            longitude: parseFloat($scope.searchLocation.longitude)
        },
        options: {
            scrollwheel: true,
            disableDefaultUI: true
        }
    };

    updateMarkers();

    function updateMarkers(){
        var markers = [];

        markers.push({
            id: 'marker_center',
            latitude: parseFloat($scope.searchLocation.latitude),
            longitude: parseFloat($scope.searchLocation.longitude),
            icon: 'images/marker-green.png'
        })

        _.each($scope.searchResult, function(item, i, arr){
            var id = 'marker_id_' + i;

            markers.push({
                id: id,
                latitude: parseFloat(item.latitude),
                longitude: parseFloat(item.longitude),
                icon: 'images/empty.png',
                options: {
                    labelContent: '<div class="p-marker ' + id + '">' +
                                    '<div class="hint">' +  
                                        '<strong>' + item.title + '</strong>' +
                                        '<a href="#" class="link-underline">Apply for Monthly Parking</a>' +
                                    '</div>' +
                                  '</div>'
                },
                events: {
                   'click': function(e){
                        document.querySelectorAll('.p-marker').forEach(function(item){
                            item.classList.remove('open');
                        });

                        document.querySelectorAll('.' + id)[0].classList.add('open');
                   } 
                }
            });
        });

        $scope.markers = markers;
    }

    //View by dropdown
    $scope.viewByDropdown = [{
        id: 'option_1',
        title: 'Option 1'
    },{
        id: 'option_2',
        title: 'Option 2'
    },{
        id: 'option_3',
        title: 'Option 3'
    }];

    $scope.selectViewType = function(item){
        $scope.selectedView = item;
    }
}]);
