app.directive('contentFullHeight', function(){
    return {
        link: linkFn
    };
 
    function linkFn(scope, element, attrs, controller) {
        //call on run app
    	calcHeight();

        //call on window resize
        window.addEventListener('resize', function(){
            calcHeight();
        }, true);

        //calculate content height
    	function calcHeight(){
            var footerH = document.querySelectorAll('#footer')[0] ? document.querySelectorAll('#footer')[0].clientHeight : 0,
                headerH = document.querySelectorAll('#header')[0] ? document.querySelectorAll('#header')[0].clientHeight : 0;

			element.css('height', (window.innerHeight - headerH - footerH) + 'px');
		}
	}
});
